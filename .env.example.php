<?php

define('HOST', 'http://localhost/rekam_medis');

define('APP_NAME', 'Rekam Medis');
define('APP_ENV', 'development');

define('DATABASE_DRIVER', 'mysqli');
define('DATABASE_HOST', 'localhost');
define('DATABASE_PORT', '3306');
define('DATABASE_USER', 'root');
define('DATABASE_PASS', '');
define('DATABASE_NAME', 'rekam_medis');
