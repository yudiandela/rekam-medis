<?php

class Berkas_model extends CI_Model {

	private $table = 'berkas';

	private $id = 'id_berkas';

	private $order_by = 'DESC';

	public function get() {
		return $this->db
			->select('berkas.*, pasien.nama_pasien as nama_pasien')
			->join('pasien', 'berkas.id_pasien = pasien.id_pasien')
			->order_by($this->id, $this->order_by)
			->get($this->table)
			->result();
	}

	public function last() {
		return $this->db->order_by($this->id, 'desc')->limit(1)->get($this->table)->row();
	}

	public function pagination($number, $offset) {
		return $this->db->order_by($this->id, $this->order_by)->get($this->table, $number, $offset)->result();
	}

	public function count() {
		return $this->db
			->join('pasien', 'berkas.id_pasien = pasien.id_pasien')
			->get($this->table)
			->num_rows();
	}

	public function store($data) {
		return $this->db->insert($this->table, $data);
	}

	public function detail($id) {
		return $this->db->where($this->id, $id)->get($this->table)->result();
	}

	public function update($id, $data) {
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	public function delete($id) {
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}
}
