<?php

class Pasien_model extends CI_Model {

	private $table = 'pasien';

	private $id = 'id_pasien';

	private $order_by = 'DESC';

	public function get() {
		return $this->db->order_by($this->id, $this->order_by)->get($this->table)->result();
	}

	public function last() {
		return $this->db->order_by($this->id, 'desc')->limit(1)->get($this->table)->row();
	}

	public function pagination($number, $offset) {
		return $this->db->order_by($this->id, $this->order_by)->get($this->table, $number, $offset)->result();
	}

	public function count() {
		return $this->db->get($this->table)->num_rows();
	}

	public function store($data) {
		return $this->db->insert($this->table, $data);
	}

	public function detail($id) {
		return $this->db->where($this->id, $id)->get($this->table)->result();
	}

	public function update($id, $data) {
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	public function delete($id) {
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}
}
