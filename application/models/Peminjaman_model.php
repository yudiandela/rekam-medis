<?php

class Peminjaman_model extends CI_Model {

	private $table = 'peminjaman';

	private $id = 'id_peminjaman';

	private $order_by = 'DESC';

	public function get($limit = false) {
		return $this->db
			->select('peminjaman.*, peminjam.nama as nama_peminjam, berkas.nomor_rekam_medis as nomor_rekam_medis')
			->join('peminjam', 'peminjaman.id_peminjam = peminjam.id_peminjam')
			->join('berkas', 'peminjaman.id_berkas = berkas.id_berkas')
			->order_by($this->id, $this->order_by)
			->limit($limit ?: 1000)
			->get($this->table)
			->result();
	}

	public function last() {
		return $this->db->order_by($this->id, 'desc')->limit(1)->get($this->table)->row();
	}

	public function pagination($number, $offset) {
		return $this->db->order_by($this->id, $this->order_by)->get($this->table, $number, $offset)->result();
	}

	public function count($join = true) {
		$db = $this->db;

		if ($join) {
			$db->join('peminjam', 'peminjaman.id_peminjam = peminjam.id_peminjam')
			   ->join('berkas', 'peminjaman.id_berkas = berkas.id_berkas');
		}

		return $db->get($this->table)->num_rows();
	}

	public function store($data) {
		return $this->db->insert($this->table, $data);
	}

	public function detail($id) {
		return $this->db->where($this->id, $id)->get($this->table)->result();
	}

	public function update($id, $data) {
		$this->db->where($this->id, $id);
		$this->db->update($this->table, $data);
	}

	public function delete($id) {
		$this->db->where($this->id, $id);
		$this->db->delete($this->table);
	}
}
