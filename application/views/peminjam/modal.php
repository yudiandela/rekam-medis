<!-- Modal -->
<div id="modalPeminjam" class="modal fade" role="dialog" aria-labelledby="modalPeminjamLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="modalPeminjamForm" action="<?= base_url('peminjam/store') ?>" method="POST">
				<div class="modal-header d-flex align-items-center">
					<h5 class="modal-title" id="modalPeminjamLabel"><span>Tambah</span> data peminjam</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="nama">Nama</label>
								<input type="text" name="nama" class="form-control" id="nama" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" name="username" class="form-control" id="username" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="telepon">Telepon</label>
								<input type="text" name="telepon" class="form-control" id="telepon" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="tanggal_lahir">Tanggal Lahir</label>
								<input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="bagian">Bagian</label>
								<select name="bagian" id="bagian" class="custom-select" required>
									<option value="Farmasi">Farmasi</option>
									<option value="SDM">SDM</option>
									<option value="Keperawatan">Keperawatan</option>
									<option value="Admin">Admin</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" name="password" id="password" class="form-control" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="repeat_password">Ulangi Password</label>
								<input type="password" name="repeat_password" id="repeat_password" class="form-control" required/>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm text-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-warning">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End Modal -->
