<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Peminjam</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Peminjam</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<!-- Card -->
		<div class="card mb-5">
			<!-- Card Header -->
			<header class="card-header d-flex justify-content-between align-items-center">
				<h2 class="h4 card-header-title">Data Peminjam</h2>
				<button class="tambah btn btn-warning" type="button"
					data-toggle="modal"
					data-target="#modalPeminjam"
					data-url="peminjam"
					>
					Tambah Data
				</button>
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body pt-0">
				<!-- Table -->
				<div class="table-responsive">
					<table class="table table-hover mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Telepon</th>
								<th>Tanggal Lahir</th>
								<th>Bidang</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$no = 1;
							foreach ($peminjam as $data):
							?>
								<tr>
									<td class="font-weight-semi-bold"><?= $no++ ?></td>
									<td class="font-weight-semi-bold"><?= $data->username ?></td>
									<td class="font-weight-semi-bold"><?= $data->nama ?></td>
									<td class="font-weight-semi-bold"><?= $data->telepon ?></td>
									<td class="font-weight-semi-bold"><?= date("d/m/Y", strtotime($data->tanggal_lahir)) ?></td>
									<td class="font-weight-semi-bold"><?= $data->bagian ?></td>
									<td>
										<!-- <a href="<?= base_url('peminjam/show/' . $data->id_peminjam) ?>" class="btn mr-2 p-0 text-secondary">Lihat</a> -->

										<button class="btn mx-2 p-0 text-info edit"
											data-target="#modalPeminjam"
											data-id="<?= $data->id_peminjam ?>"
											data-username="<?= $data->username ?>"
											data-nama="<?= $data->nama ?>"
											data-telepon="<?= $data->telepon ?>"
											data-tanggal_lahir="<?= $data->tanggal_lahir ?>"
											data-bagian="<?= $data->bagian ?>"
											data-url="peminjam"
										>
											Edit
										</button>

										<?php if ($this->session->userdata('id') !== $data->id_peminjam): ?>
											<a href="<?= base_url('peminjam/delete/' . $data->id_peminjam) ?>" class="btn ml-2 p-0 text-danger" onclick="return confirm('Yakin akan menghapus data ini?')">Hapus</a>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<!-- End Table -->
			</div>
			<!-- Card Body -->
		</div>
		<!-- End Card -->
	</div>
</div>
<!-- End Content Body -->
