<?php $this->load->view('layout/header') ?>
<!-- Main -->
<main class="u-error-content-wrap mt-5">
	<div class="u-error-content container text-center my-auto">
		<h1 class="u-error__title">404</h1>
		<h2 class="u-error__sub-title">Page not found</h2>
		<h4 class="font-weight-semi-bold mb-0">
			<a href="<?= base_url() ?>">Go to dashboard</a>
		</h4>

		<!-- SVG Shapes -->
		<figure class="u-shape u-shape-top-left">
			<img src="<?= base_url('assets/svg/shapes/shape-1.svg') ?>" alt="Image description">
		</figure>
		<figure class="u-shape u-shape-top-right">
			<img src="<?= base_url('assets/svg/shapes/shape-2.svg') ?>" alt="Image description">
		</figure>
		<figure class="u-shape u-shape-bottom-left">
			<img src="<?= base_url('assets/svg/shapes/shape-3.svg') ?>" alt="Image description">
		</figure>
		<figure class="u-shape u-shape-bottom-right">
			<img src="<?= base_url('assets/svg/shapes/shape-4.svg') ?>" alt="Image description">
		</figure>
		<!-- End SVG Shapes -->
	</div>
</main>
<!-- End Main -->

<?php $this->load->view('layout/footer') ?>
