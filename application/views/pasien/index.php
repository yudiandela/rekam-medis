<?php $this->load->view('layout/header') ?>

<?php $this->load->view('layout/navbar') ?>

<main class="u-main">
	<?php $this->load->view('layout/sidebar') ?>

	<!-- Content -->
	<div class="u-content">
		<?php $this->load->view('pasien/content') ?>

		<?php $this->load->view('layout/footer_content') ?>
	</div>
	<!-- End Content -->
</main>

<?php $this->load->view('pasien/modal') ?>

<?php $this->load->view('layout/footer') ?>
