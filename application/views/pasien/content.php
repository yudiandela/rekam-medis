<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Pasien</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Pasien</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<!-- Card -->
		<div class="card mb-5">
			<!-- Card Header -->
			<header class="card-header d-flex justify-content-between align-items-center">
				<h2 class="h4 card-header-title">Data Pasien</h2>
				<button class="tambah btn btn-warning" type="button"
					data-toggle="modal"
					data-target="#modalPasien"
					data-url="pasien"
					>
					Tambah Data
				</button>
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body pt-0">
				<!-- Table -->
				<div class="table-responsive">
					<table class="table table-hover mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama</th>
								<th>Telepon</th>
								<th>Tanggal Lahir</th>
								<th>Jenis Kelamin</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$no = 1;
							foreach ($pasien as $data):
							?>
								<tr>
									<td class="font-weight-semi-bold"><?= $no++ ?></td>
									<td class="font-weight-semi-bold"><?= $data->nama_pasien ?></td>
									<td class="font-weight-semi-bold"><?= $data->telepon ?></td>
									<td class="font-weight-semi-bold"><?= date("d/m/Y", strtotime($data->tanggal_lahir)) ?></td>
									<td class="font-weight-semi-bold"><?= $data->jenis_kelamin ?></td>
									<td>
										<button class="btn mr-2 p-0 text-info edit"
											data-target="#modalPasien"
											data-id="<?= $data->id_pasien ?>"
											data-nama_pasien="<?= $data->nama_pasien ?>"
											data-telepon="<?= $data->telepon ?>"
											data-tanggal_lahir="<?= $data->tanggal_lahir ?>"
											data-jenis_kelamin="<?= $data->jenis_kelamin ?>"
											data-url="pasien"
										>
											Edit
										</button>
										<a href="<?= base_url('pasien/delete/' . $data->id_pasien) ?>" class="btn ml-2 p-0 text-danger" onclick="return confirm('Yakin akan menghapus data ini?')">Hapus</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<!-- End Table -->
			</div>
			<!-- Card Body -->
		</div>
		<!-- End Card -->
	</div>
</div>
<!-- End Content Body -->
