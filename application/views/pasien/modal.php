<!-- Modal -->
<div id="modalPasien" class="modal fade" role="dialog" aria-labelledby="modalPasienLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="modalPasienForm" action="<?= base_url('pasien/store') ?>" method="POST">
				<div class="modal-header d-flex align-items-center">
					<h5 class="modal-title" id="modalPasienLabel"><span>Tambah</span> data pasien</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label for="nama_pasien">Nama Pasien</label>
						<input type="text" name="nama_pasien" class="form-control" id="nama_pasien" required/>
					</div>

					<div class="form-group">
						<label for="no_telepon">No Telepon</label>
						<input type="text" name="no_telepon" class="form-control" id="no_telepon" required/>
					</div>

					<div class="form-group">
						<label for="tanggal_lahir">Tanggal Lahir</label>
						<input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" required/>
					</div>

					<div class="form-group">
						<label for="jenis_kelamin">Jenis Kelamin</label>
						<select name="jenis_kelamin" id="jenis_kelamin" class="custom-select" required>
							<option value="Laki-laki">Laki-laki</option>
							<option value="Perempuan">Perempuan</option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm text-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-warning">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End Modal -->
