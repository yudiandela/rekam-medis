<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Profile</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Profile</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<!-- Row -->
		<div class="row">
			<!-- Coloum -->
			<div class="col-md-12">
				<!-- Card -->
				<div class="card mb-5">
					<!-- Card Header -->
					<header class="card-header d-flex justify-content-between align-items-center">
						<h2 class="h4 card-header-title">My Profile</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<form action="<?= base_url('profile/update') ?>" method="POST">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="nama">Nama</label>
										<input type="text" name="nama" class="form-control" id="nama" value="<?= $profile[0]->nama ?>"/>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="username">Username</label>
										<input type="text" name="username" class="form-control" id="username" value="<?= $profile[0]->username ?>"/>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="telepon">Telepon</label>
										<input type="text" name="telepon" class="form-control" id="telepon" value="<?= $profile[0]->telepon ?>"/>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="tanggal_lahir">Tanggal Lahir</label>
										<input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir" value="<?= $profile[0]->tanggal_lahir ?>"/>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="bagian">Bagian</label>
										<select name="bagian" id="bagian" class="custom-select">
											<option <?= $profile[0]->bagian == 'Farmasi' ? 'selected' : ''  ?> value="Farmasi">Farmasi</option>
											<option <?= $profile[0]->bagian == 'SDM' ? 'selected' : ''  ?> value="SDM">SDM</option>
											<option <?= $profile[0]->bagian == 'Keperawatan' ? 'selected' : ''  ?> value="Keperawatan">Keperawatan</option>
										</select>
									</div>
								</div>

								<div class="col-md-12">
									<button type="submit" class="btn btn-warning float-right">Simpan Perubahan</button>
								</div>
							</div>
						</form>
					</div>
					<!-- Card Body -->
				</div>
				<!-- End Card -->
			</div>
			<!-- End Coloum -->
		</div>
		<!-- End Row -->
	</div>
</div>
<!-- End Content Body -->
