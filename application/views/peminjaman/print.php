<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  padding: 5px;
}
</style>

<!-- Content Body -->
<div style="text-align: center;">
	<h1>Amalia Medical Center</h1>
	<p>Jl. Dewi Sartika No.364A, RT.3/RW.4, Cawang, Kec. Kramat jati, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13630</p>
	<p>Kramat Jati, Jakarta Timur</p>

	<hr>

	<!-- Card -->
	<div>
		<!-- Card Header -->
		<header>
			<h2>Data Peminjaman</h2>
			<p><?= date('d F Y') ?></p>
		</header>
		<!-- End Card Header -->

		<!-- Card Body -->
		<div>
			<!-- Table -->
			<div>
				<table style="width: 100%;">
					<thead>
						<tr>
							<th>#</th>
							<th>Tanggal</th>
							<th>Nomor</th>
							<th>Berkas</th>
							<th>Bagian</th>
							<th>Keterangan</th>
							<th>Peminjam</th>
							<th>Status</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$no = 1;
						foreach ($peminjaman as $data):
						?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= date("d/m/Y", strtotime($data->tanggal)) ?></td>
								<td><?= $data->nomor_peminjaman ?></td>
								<td><?= $data->nomor_rekam_medis ?></td>
								<td><?= $data->bagian ?></td>
								<td><?= $data->keterangan ?></td>
								<td><?= $data->nama_peminjam ?></td>
								<td><?= $data->status ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<!-- End Table -->
		</div>
		<!-- Card Body -->

		<footer style="margin-top: 40px; width: 200px; float: right">
			Jakarta, <?= date('d F Y') ?>
			<br>
			<?= $this->session->userdata('nama') ?>
			<br>
			<br>
			<br>
			(.......................................)
		</footer>
	</div>
	<!-- End Card -->
</div>
<!-- End Content Body -->
