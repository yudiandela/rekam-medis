<!-- Modal -->
<div id="modalPeminjaman" class="modal fade" role="dialog" aria-labelledby="modalPeminjamanLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="modalPeminjamanForm" action="<?= base_url('peminjaman/store') ?>" method="POST">
				<div class="modal-header d-flex align-items-center">
					<h5 class="modal-title" id="modalPeminjamanLabel"><span>Tambah</span> data peminjaman</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label for="pilihPeminjam">Nama Peminjam</label>
						<select name="peminjam" id="pilihPeminjam" class="custom-select" required>
							<?php foreach ($peminjam as $data): ?>
								<option value="<?= $data->id_peminjam ?>">
									<?= $data->nama ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<label for="pilihBerkas">Pilih Berkas</label>
						<select name="id_berkas" id="pilihBerkas" class="custom-select" required>
							<?php foreach ($berkas as $data): ?>
								<option value="<?= $data->id_berkas ?>">
									<?= $data->nomor_rekam_medis ?> : <?= $data->nama_pasien ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>

					<div class="form-group">
						<label for="tanggal">Tanggal</label>
						<input type="date" name="tanggal" class="form-control" id="tanggal" required/>
					</div>

					<div class="form-group">
						<label for="bagian">Bagian</label>
						<select name="bagian" id="bagian" class="custom-select" required>
							<option value="Sdm">Sdm</option>
							<option value="Keperawatan">Keperawatan</option>
							<option value="Farmasi">Farmasi</option>
						</select>
					</div>

					<div class="form-group">
						<label for="keterangan">Keterangan</label>
						<textarea name="keterangan" id="keterangan" class="form-control" rows="3" required></textarea>
					</div>

					<div class="form-group">
						<label for="status">Status</label>
						<select name="status" id="status" class="custom-select" required>
							<option value="Masih Dipinjam">Masih Dipinjam</option>
							<option value="Sudah Dikembalikan">Sudah Dikembalikan</option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm text-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-warning">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End Modal -->
