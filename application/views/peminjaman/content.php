<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Peminjaman</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Peminjaman</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<!-- Card -->
		<div class="card mb-5">
			<!-- Card Header -->
			<header class="card-header d-flex justify-content-between align-items-center">
				<h2 class="h4 card-header-title">Data Peminjaman</h2>

				<div>
					<button class="print btn btn-info" type="button" onclick="printPage('<?= base_url('peminjaman/cetak') ?>');">
						Print
					</button>
					<button class="tambah btn btn-warning" type="button"
						data-toggle="modal"
						data-target="#modalPeminjaman"
						data-url="peminjaman"
						>
						Tambah Data
					</button>
				</div>
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body pt-0">
				<!-- Table -->
				<div class="table-responsive">
					<table class="table table-hover mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>Berkas</th>
								<th>Bagian</th>
								<th>Keterangan</th>
								<th>Peminjam</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$no = 1;
							foreach ($peminjaman as $data):
							?>
								<tr>
									<td class="font-weight-semi-bold"><?= $no++ ?></td>
									<td class="font-weight-semi-bold"><?= date("d/m/Y", strtotime($data->tanggal)) ?></td>
									<td class="font-weight-semi-bold"><?= $data->nomor_peminjaman ?></td>
									<td class="font-weight-semi-bold"><?= $data->nomor_rekam_medis ?></td>
									<td class="font-weight-semi-bold"><?= $data->bagian ?></td>
									<td class="font-weight-semi-bold"><?= $data->keterangan ?></td>
									<td class="font-weight-semi-bold"><?= $data->nama_peminjam ?></td>
									<td class="font-weight-semi-bold"><?= $data->status ?></td>
									<td>
										<button class="btn mr-2 p-0 text-info edit"
											data-target="#modalPeminjaman"
											data-id="<?= $data->id_peminjaman ?>"
											data-status="<?= $data->status ?>"
											data-bagian="<?= $data->bagian ?>"
											data-keterangan="<?= $data->keterangan ?>"
											data-tanggal="<?= $data->tanggal ?>"
											data-id_berkas="<?= $data->id_berkas ?>"
											data-url="peminjaman"
										>
											Edit
										</button>
										<a href="<?= base_url('peminjaman/delete/' . $data->id_peminjaman) ?>" class="btn ml-2 p-0 text-danger" onclick="return confirm('Yakin akan menghapus data ini?')">Hapus</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<!-- End Table -->
			</div>
			<!-- Card Body -->
		</div>
		<!-- End Card -->
	</div>
</div>
<!-- End Content Body -->
