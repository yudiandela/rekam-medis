<!-- Modal -->
<div id="modalBerkas" class="modal fade" role="dialog" aria-labelledby="modalBerkasLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form id="modalBerkasForm" action="<?= base_url('berkas/store') ?>" method="POST">
				<div class="modal-header d-flex align-items-center">
					<h5 class="modal-title" id="modalBerkasLabel"><span>Tambah</span> data berkas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="id_pasien">Pasien</label>
								<select name="id_pasien" id="id_pasien" class="custom-select" required>
									<option value=""></option>
									<?php foreach ($pasien as $data): ?>
										<option value="<?= $data->id_pasien ?>">
											<?= $data->nama_pasien ?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="nomor_rekam_medis">Nomor Rekam Medis</label>
								<input type="text" name="nomor_rekam_medis" class="form-control" id="nomor_rekam_medis" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="tanggal_rekam_medis">Tanggal Rekam Medis</label>
								<input type="date" name="tanggal_rekam_medis" class="form-control" id="tanggal_rekam_medis" required/>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="resume_keperawatan">Resume Keperawatan</label>
								<textarea name="resume_keperawatan" class="form-control" id="resume_keperawatan" required></textarea>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="data_obat">Data Obat</label>
								<textarea name="data_obat" class="form-control" id="data_obat" required></textarea>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label for="data_diagnosa">Data Diagnosa</label>
								<textarea name="data_diagnosa" class="form-control" id="data_diagnosa" required></textarea>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm text-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-warning">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End Modal -->
