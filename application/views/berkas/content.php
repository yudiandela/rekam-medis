<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Berkas</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Berkas</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<!-- Card -->
		<div class="card mb-5">
			<!-- Card Header -->
			<header class="card-header d-flex justify-content-between align-items-center">
				<h2 class="h4 card-header-title">Data Berkas</h2>
				<button class="tambah btn btn-warning" type="button"
					data-toggle="modal"
					data-target="#modalBerkas"
					data-url="berkas"
					>
					Tambah Data
				</button>
			</header>
			<!-- End Card Header -->

			<!-- Card Body -->
			<div class="card-body pt-0">
				<!-- Table -->
				<div class="table-responsive">
					<table class="table table-hover mb-0">
						<thead>
							<tr>
								<th>#</th>
								<th>No Rekam Medis</th>
								<th>Tanggal Rekam</th>
								<th>Resume Keperawatan</th>
								<th>Data Obat</th>
								<th>Data Diagnosa</th>
								<th>Pasien</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$no = 1;
							foreach ($berkas as $data):
							?>
								<tr>
									<td class="font-weight-semi-bold align-top"><?= $no++ ?></td>
									<td class="font-weight-semi-bold align-top"><?= $data->nomor_rekam_medis ?></td>
									<td class="font-weight-semi-bold align-top"><?= date("d/m/Y", strtotime($data->tanggal_rekam_medis)) ?></td>
									<td class="font-weight-semi-bold align-top"><?= $data->resume_keperawatan ?></td>
									<td class="font-weight-semi-bold align-top"><?= $data->data_obat ?></td>
									<td class="font-weight-semi-bold align-top"><?= $data->data_diagnosa ?></td>
									<td class="font-weight-semi-bold align-top"><?= $data->nama_pasien ?></td>
									<td class="align-top">
										<button class="btn mr-2 p-0 text-info edit"
											data-target="#modalBerkas"
											data-id="<?= $data->id_berkas ?>"
											data-nomor_rekam_medis="<?= $data->nomor_rekam_medis ?>"
											data-tanggal_rekam_medis="<?= $data->tanggal_rekam_medis ?>"
											data-resume_keperawatan="<?= $data->resume_keperawatan ?>"
											data-data_obat="<?= $data->data_obat ?>"
											data-data_diagnosa="<?= $data->data_diagnosa ?>"
											data-id_pasien="<?= $data->id_pasien ?>"
											data-url="berkas"
										>
											Edit
										</button>
										<a href="<?= base_url('berkas/delete/' . $data->id_berkas) ?>" class="btn ml-2 p-0 text-danger" onclick="return confirm('Yakin akan menghapus data ini?')">Hapus</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<!-- End Table -->
			</div>
			<!-- Card Body -->
		</div>
		<!-- End Card -->
	</div>
</div>
<!-- End Content Body -->
