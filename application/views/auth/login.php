<?php $this->load->view('layout/header') ?>

<!-- Main -->
<main class="d-flex flex-column u-hero u-hero--end mnh-100vh" style="background-image: url(assets/img-temp/bg/bg-1.png);">
	<div class="container py-11 my-auto">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-6 col-lg-5 mb-4 mb-md-0">
				<!-- Card -->
				<div class="card">
					<!-- Card Body -->
					<div class="card-body p-4 p-lg-7">
						<h2 class="text-center mb-4">Login</h2>

						<?= $this->session->flashdata('message') ?>

						<!-- Sign in Form -->
						<form action="<?= base_url('auth/login') ?>" method="POST">
							<!-- Email -->
							<div class="form-group">
								<label for="inputUsername">Username</label>
								<input type="text" name="username" id="inputUsername" class="form-control" placeholder="username" value="<?= set_value('username'); ?>" autofocus>
								<?= form_error('username'); ?>
							</div>
							<!-- End Email -->

							<!-- Password -->
							<div class="form-group">
								<label for="inputPassword">Password</label>
								<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Enter your password">
							</div>
							<!-- End Password -->

							<div class="d-flex align-items-center justify-content-between my-4">
								<!-- Remember -->
								<div class="custom-control custom-checkbox">
									<input id="rememberMe" class="custom-control-input" type="checkbox">
									<label class="custom-control-label text-dark" for="rememberMe">Remember me</label>
								</div>
								<!-- End Remember -->
							</div>

							<button type="submit" class="btn btn-block btn-wide btn-warning text-uppercase">Login</button>
						</form>
						<!-- End Sign in Form -->
					</div>
					<!-- End Card Body -->
				</div>
				<!-- End Card -->
			</div>
		</div>
	</div>
</main>
<!-- End Main -->

<?php $this->load->view('layout/footer') ?>
