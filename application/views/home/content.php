<!-- Content Body -->
<div class="u-body">
	<div class="mb-4">
		<h1 class="h2 mb-2">Dashboard</h1>

		<!-- Breadcrumb -->
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="<?= base_url() ?>">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
			</ol>
		</nav>
		<!-- End Breadcrumb -->

		<?= $this->session->flashdata('message') ?>

		<div class="row">
			<div class="col-md-4">
				<!-- Card -->
				<div class="card mb-5">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Jumlah Pasien</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<p id="jumlah_pasien" style="font-size: 34px;" class="h2 p-0">0</p>
					</div>
					<!-- Card Body -->
				</div>
				<!-- End Card -->
			</div>
			<div class="col-md-4">
				<!-- Card -->
				<div class="card mb-5">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Jumlah Berkas</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<p id="jumlah_berkas" style="font-size: 34px;" class="h2 p-0">0</p>
					</div>
					<!-- Card Body -->
				</div>
				<!-- End Card -->
			</div>
			<div class="col-md-4">
				<!-- Card -->
				<div class="card mb-5">
					<!-- Card Header -->
					<header class="card-header">
						<h2 class="h4 card-header-title">Jumlah Peminjaman</h2>
					</header>
					<!-- End Card Header -->

					<!-- Card Body -->
					<div class="card-body pt-0">
						<p id="jumlah_peminjaman" style="font-size: 34px;" class="h2 p-0">0</p>
					</div>
					<!-- Card Body -->
				</div>
				<!-- End Card -->
			</div>
		</div>
	</div>
</div>
<!-- End Content Body -->
