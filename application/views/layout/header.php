<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">

	<title><?= APP_NAME ?></title>

	<!-- Web Fonts -->
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">

	<!-- Components Vendor Styles -->
	<link rel="stylesheet" href="<?= base_url('assets/vendor/themify-icons/themify-icons.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/DataTables-1.11.0/datatables.min.css') ?>">

	<!-- Theme Styles -->
	<link rel="stylesheet" href="<?= base_url('assets/css/theme.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">
</head>
<body>
