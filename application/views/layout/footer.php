<?php $this->load->view('layout/partials/modal') ?>

<!-- Global Vendor -->
<script src="<?= base_url('assets/vendor/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-migrate/jquery-migrate.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/popper.js/dist/umd/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

<!-- Plugins -->
<script src="<?= base_url('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/DataTables-1.11.0/datatables.min.js') ?>"></script>

<!-- Initialization  -->
<script src="<?= base_url('assets/js/sidebar-nav.js') ?>"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script>
	$(document).ready(function() {
		$('table').DataTable();

		$('.edit').on('click', function() {

			$('input[type="password"]').removeAttr('required')

			const target = $(this).data('target')
			const id = $(this).data('id')
			const url = $(this).data('url')

			// Data Peminjaman
			const keterangan = $(this).data('keterangan')
			const tanggal = $(this).data('tanggal')
			const id_berkas = $(this).data('id_berkas')
			const status = $(this).data('status')

			// Data Pasien
			const nama_pasien = $(this).data('nama_pasien')
			const telepon = $(this).data('telepon')
			const tanggal_lahir = $(this).data('tanggal_lahir')
			const jenis_kelamin = $(this).data('jenis_kelamin')

			// Data Berkas
			const nomor_rekam_medis = $(this).data('nomor_rekam_medis')
			const tanggal_rekam_medis = $(this).data('tanggal_rekam_medis')
			const resume_keperawatan = $(this).data('resume_keperawatan')
			const data_obat = $(this).data('data_obat')
			const data_diagnosa = $(this).data('data_diagnosa')
			const id_pasien = $(this).data('id_pasien')

			// Data Peminjam
			const username = $(this).data('username')
			const nama = $(this).data('nama')
			const bagian = $(this).data('bagian')
			const password = $(this).data('password')
			const repeat_password = $(this).data('repeat_password')

			$(target).modal('show')

			// Update Data Peminjaman
			$('#pilihBerkas').val(id_berkas)
			$('#tanggal').val(tanggal)
			$('#keterangan').val(keterangan)
			$('#status').val(status)

			// Update Data Pasien
			$('#nama_pasien').val(nama_pasien)
			$('#no_telepon').val(telepon)
			$('#tanggal_lahir').val(tanggal_lahir)
			$('#jenis_kelamin').val(jenis_kelamin)

			// Update Data Berkas
			$('#nomor_rekam_medis').val(nomor_rekam_medis)
			$('#tanggal_rekam_medis').val(tanggal_rekam_medis)
			$('#resume_keperawatan').val(resume_keperawatan)
			$('#data_obat').val(data_obat)
			$('#data_diagnosa').val(data_diagnosa)
			$('#id_pasien').val(id_pasien)

			// Update Data Peminjam
			$('#username').val(username)
			$('#nama').val(nama)
			$('#telepon').val(telepon)
			$('#tanggal_lahir').val(tanggal_lahir)
			$('#bagian').val(bagian)
			$('#password').val('')
			$('#repeat_password').val('')

			$(`${target}Form`).attr('action', `<?= base_url('${url}/update/${id}') ?>`)
			$(`${target}Form > .modal-footer > .btn-warning`).text('Update')
			$(`${target}Label > span`).text('Update')
		})

		$('.tambah').on('click', function() {

			$('input[type="password"]').attr('required')

			const url = $(this).data('url')
			const target = $(this).data('target')

			// Update Data Peminjaman
			$('#pilihBerkas').val('')
			$('#tanggal').val('')
			$('#keterangan').val('')

			// Update Data Pasien
			$('#nama_pasien').val('')
			$('#no_telepon').val('')
			$('#tanggal_lahir').val('')
			$('#jenis_kelamin').val('')

			// Update Data Berkas
			$('#nomor_rekam_medis').val('')
			$('#tanggal_rekam_medis').val('')
			$('#resume_keperawatan').val('')
			$('#data_obat').val('')
			$('#data_diagnosa').val('')
			$('#id_pasien').val('')

			// Update Data Peminjam
			$('#username').val('')
			$('#nama').val('')
			$('#telepon').val('')
			$('#tanggal_lahir').val('')
			$('#bagian').val('')
			$('#password').val('')
			$('#repeat_password').val('')

			$(`${target}Form`).attr('action', `<?= base_url('${url}/store') ?>`)
			$(`${target}Form > .modal-footer > .btn-warning`).text('Tambah')
			$(`${target}Label > span`).text('Tambah')
		})

		function getData(url, target) {
			$.ajax({
				method: 'GET',
				url: url,
				success: function(res) {
					$(target).text(res)
				},
				error: function(err) {
					console.log(err)
				}
			})
		}

		getData('<?= base_url('peminjaman/count') ?>', '#jumlah_peminjaman')
		getData('<?= base_url('pasien/count') ?>', '#jumlah_pasien')
		getData('<?= base_url('berkas/count') ?>', '#jumlah_berkas')
	});

	function closePrint () {
		document.body.removeChild(this.__container__);
	}

	function setPrint () {
		this.contentWindow.__container__ = this;
		this.contentWindow.onbeforeunload = closePrint;
		this.contentWindow.onafterprint = closePrint;
		this.contentWindow.focus(); // Required for IE
		this.contentWindow.print();
	}

	function printPage (sURL) {
		var oHideFrame = document.createElement("iframe");
		oHideFrame.onload = setPrint;
		oHideFrame.style.position = "fixed";
		oHideFrame.style.right = "0";
		oHideFrame.style.bottom = "0";
		oHideFrame.style.width = "0";
		oHideFrame.style.height = "0";
		oHideFrame.style.border = "0";
		oHideFrame.src = sURL;
		document.body.appendChild(oHideFrame);
	}
</script>
</body>
</html>
