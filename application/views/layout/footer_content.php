<!-- Footer -->
<footer class="u-footer d-md-flex align-items-md-center text-center text-md-left text-muted">
	<!-- Footer Menu -->
	<!-- <ul class="list-inline mb-3 mb-md-0">
		<li class="list-inline-item mr-4">
			<a class="text-muted" href="">

			</a>
		</li>
		<li class="list-inline-item">
			<a class="text-muted" href="">

			</a>
		</li>
	</ul> -->
	<!-- End Footer Menu -->

	<!-- Copyright -->
	<span class="text-muted ml-auto">&copy; <?= date('Y') ?> <a class="text-muted" href="<?= base_url() ?>"><?= APP_NAME ?></a>. All Rights Reserved.</span>
	<!-- End Copyright -->
</footer>
<!-- End Footer -->
