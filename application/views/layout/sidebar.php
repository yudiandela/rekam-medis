<!-- Sidebar -->
<aside id="sidebar" class="u-sidebar">
	<div class="u-sidebar-inner">
		<!-- Sidebar Header -->
		<header class="u-sidebar-header">
			<!-- Sidebar Logo -->
			<a class="u-sidebar-logo" href="<?= base_url() ?>">
				<?= APP_NAME ?>
			</a>
			<!-- End Sidebar Logo -->
		</header>
		<!-- End Sidebar Header -->

		<!-- Sidebar Nav -->
		<nav class="u-sidebar-nav">
			<!-- Sidebar Nav Menu -->
			<ul class="u-sidebar-nav-menu u-sidebar-nav-menu--top-level">
				<!-- Dashboard -->
				<li class="u-sidebar-nav-menu__item">
					<a class="u-sidebar-nav-menu__link" href="<?= base_url() ?>">
						<span class="ti-dashboard u-sidebar-nav-menu__item-icon"></span>
						<span class="u-sidebar-nav-menu__item-title">Dashboard</span>
					</a>
				</li>
				<!-- End Dashboard -->

				<!-- Dashboard -->
				<li class="u-sidebar-nav-menu__item">
					<a class="u-sidebar-nav-menu__link" href="<?= base_url('peminjaman') ?>">
						<span class="ti-notepad u-sidebar-nav-menu__item-icon"></span>
						<span class="u-sidebar-nav-menu__item-title">Peminjaman</span>
					</a>
				</li>
				<!-- End Dashboard -->

				<!-- Pasien -->
				<li class="u-sidebar-nav-menu__item">
					<a class="u-sidebar-nav-menu__link" href="<?= base_url('pasien') ?>">
						<span class="ti-user u-sidebar-nav-menu__item-icon"></span>
						<span class="u-sidebar-nav-menu__item-title">Pasien</span>
					</a>
				</li>
				<!-- End Pasien -->

				<!-- Berkas -->
				<li class="u-sidebar-nav-menu__item">
					<a class="u-sidebar-nav-menu__link" href="<?= base_url('berkas') ?>">
						<span class="ti-blackboard u-sidebar-nav-menu__item-icon"></span>
						<span class="u-sidebar-nav-menu__item-title">Berkas</span>
					</a>
				</li>
				<!-- End Berkas -->

				<li class="u-sidebar-nav-menu__divider"></li>

				<!-- Peminjam -->
				<li class="u-sidebar-nav-menu__item">
					<a class="u-sidebar-nav-menu__link" href="<?= base_url('peminjam') ?>">
						<span class="ti-id-badge u-sidebar-nav-menu__item-icon"></span>
						<span class="u-sidebar-nav-menu__item-title">Peminjam</span>
					</a>
				</li>
				<!-- End Peminjam -->
			</ul>
			<!-- End Sidebar Nav Menu -->
		</nav>
		<!-- End Sidebar Nav -->
	</div>
</aside>
<!-- End Sidebar -->
