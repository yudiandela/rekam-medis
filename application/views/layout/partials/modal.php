<!-- Modal -->
<div id="modalPassword" class="modal fade" role="dialog" aria-labelledby="modalPasswordLabel" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<form id="modalPasswordForm" action="<?= base_url('profile/password') ?>" method="POST">
				<div class="modal-header d-flex align-items-center">
					<h5 class="modal-title" id="modalPasswordLabel">Ubah Password</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="password_lama">Password Lama</label>
								<input type="password" name="password_lama" class="form-control" id="password_lama"/>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label for="password_baru">Password Baru</label>
								<input type="password" name="password_baru" class="form-control" id="password_baru"/>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<label for="password_baru_konfirmasi">Ulangi Password Baru</label>
								<input type="password" name="password_baru_konfirmasi" class="form-control" id="password_baru_konfirmasi"/>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-sm text-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-warning">Ganti Password</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End Modal -->
