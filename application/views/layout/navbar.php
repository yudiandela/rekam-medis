<!-- Header (Topbar) -->
<header class="u-header">
	<!-- Header Left Section -->
	<div class="u-header-left bg-warning">
		<!-- Header Logo -->
		<a class="u-header-logo text-dark" href="<?= base_url() ?>">
			<img class="u-header-logo__icon" src="<?= base_url('assets/img/logo-medic.svg') ?>" width="20px" height="20px" alt="Awesome Icon">
			<span class="u-header-logo__text text-uppercase mt-1"><?= APP_NAME ?></span>
		</a>
		<!-- End Header Logo -->
	</div>
	<!-- End Header Left Section -->

	<!-- Header Middle Section -->
	<div class="u-header-middle d-flex justify-content-between">
		<!-- Sidebar Invoker -->
		<div class="u-header-section">
			<a class="js-sidebar-invoker u-header-invoker u-sidebar-invoker" href="#"
			   data-is-close-all-except-this="true"
			   data-target="#sidebar">
				<span class="ti-align-left u-header-invoker__icon u-sidebar-invoker__icon--open"></span>
				<span class="ti-align-justify u-header-invoker__icon u-sidebar-invoker__icon--close"></span>
			</a>
		</div>
		<!-- End Sidebar Invoker -->

		<!-- User Profile -->
		<div class="u-header-section u-header-section--profile">
			<div class="u-header-dropdown dropdown">
				<a class="link-muted d-flex align-items-center" href="#" role="button" id="userProfileInvoker" aria-haspopup="true" aria-expanded="false"
				   data-toggle="dropdown"
				   data-offset="0">
					<span class="text-dark d-none d-md-inline-flex align-items-center">
						<?= $this->session->userdata('nama') ?>
						<span class="ti-angle-down text-muted ml-4"></span>
					</span>
				</a>

				<div class="u-header-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="userProfileInvoker" style="width: 260px;">
					<div class="card p-3">
						<div class="card-body p-0">
							<ul class="list-unstyled mb-0">
								<li class="mb-3">
									<a class="link-dark d-block" href="<?= base_url('profile') ?>">
										<span class="ti-settings mr-2"></span>
										Profile
									</a>
								</li>
								<li class="mb-3">
									<a class="link-dark d-block" href="javascript:void(0)"
										data-toggle="modal" data-target="#modalPassword">
										<span class="ti-key mr-2"></span>
										Password
									</a>
								</li>
								<li>
									<a class="link-dark d-block" href="<?= base_url('auth/logout') ?>">
										<span class="ti-power-off mr-2"></span>
										Keluar
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End User Profile -->
	</div>
	<!-- End Header Middle Section -->
</header>
<!-- End Header (Topbar) -->
