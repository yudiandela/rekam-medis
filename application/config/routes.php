<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'SiteController/index';

/** Peminjaman Route */
$route['peminjaman'] = 'PeminjamanController/index';
$route['peminjaman/count'] = 'PeminjamanController/count';
$route['peminjaman/store'] = 'PeminjamanController/store';
$route['peminjaman/cetak'] = 'PeminjamanController/cetak';
$route['peminjaman/show/(:any)'] = 'PeminjamanController/show/$1';
$route['peminjaman/update/(:any)'] = 'PeminjamanController/update/$1';
$route['peminjaman/delete/(:any)'] = 'PeminjamanController/delete/$1';

/** Pasien Route */
$route['pasien'] = 'PasienController/index';
$route['pasien/count'] = 'PasienController/count';
$route['pasien/store'] = 'PasienController/store';
$route['pasien/show/(:any)'] = 'PasienController/show/$1';
$route['pasien/update/(:any)'] = 'PasienController/update/$1';
$route['pasien/delete/(:any)'] = 'PasienController/delete/$1';

/** Berkas Route */
$route['berkas'] = 'BerkasController/index';
$route['berkas/count'] = 'BerkasController/count';
$route['berkas/store'] = 'BerkasController/store';
$route['berkas/show/(:any)'] = 'BerkasController/show/$1';
$route['berkas/update/(:any)'] = 'BerkasController/update/$1';
$route['berkas/delete/(:any)'] = 'BerkasController/delete/$1';

/** Peminjam Route */
$route['peminjam'] = 'PeminjamController/index';
$route['peminjam/count'] = 'PeminjamController/count';
$route['peminjam/store'] = 'PeminjamController/store';
$route['peminjam/show/(:any)'] = 'PeminjamController/show/$1';
$route['peminjam/update/(:any)'] = 'PeminjamController/update/$1';
$route['peminjam/delete/(:any)'] = 'PeminjamController/delete/$1';

/** Authentication Route */
$route['auth/login'] = 'AuthController/login';
$route['auth/logout'] = 'AuthController/logout';

/** Profile Route */
$route['profile'] = 'ProfileController/index';
$route['profile/password'] = 'ProfileController/password';
$route['profile/update'] = 'ProfileController/update';

/** ERROR OVERRIDE */
$route['404_override'] = 'SiteController/_404';
$route['translate_uri_dashes'] = TRUE;
