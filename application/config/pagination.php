<?php

$config['attributes'] = array('class' => 'page-link');

$config['first_link'] = '<span class="ti-angle-double-left"></span>';
$config['last_link'] = '<span class="ti-angle-double-right"></span>';

$config['full_tag_open'] = '<nav class="mt-5" aria-label="Bootstrap Pagination"><ul class="pagination justify-content-end">';
$config['full_tag_close'] = '</ul></nav>';

$config['prev_link'] = '<span class="ti-angle-left"></span>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';

$config['next_link'] = '<span class="ti-angle-right"></span>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';
