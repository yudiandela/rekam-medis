<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeminjamController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}

		/** Load Library yang di gunakan */
		$this->load->library('form_validation');

		/** Load model yang di gunakan */
		$this->load->model('peminjam_model');
	}

	public function index()
	{
		$data['peminjam'] = $this->peminjam_model->get();

		$this->load->view('peminjam/index', $data);
	}

	public function store()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('bagian', 'Bagian', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'required');

		if ($this->form_validation->run() == false) {
			redirect('peminjam');
		} else {
			if($this->input->post('password', true) == $this->input->post('repeat_password', true)) {
				$data = [
					'username'      => (string) htmlspecialchars($this->input->post('username', true)),
					'nama'          => (string) htmlspecialchars($this->input->post('nama', true)),
					'telepon'       => (string) htmlspecialchars($this->input->post('telepon', true)),
					'tanggal_lahir' => (string) htmlspecialchars($this->input->post('tanggal_lahir', true)),
					'bagian'        => (string) htmlspecialchars($this->input->post('bagian', true)),
					'password'      => (string) htmlspecialchars($this->input->post('password', true)),
				];

				$this->peminjam_model->store($data);

				$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menginputkan data peminjam!</div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-success">Gagal password konfirmasi tidak sama!</div>');
			}

			redirect('peminjam');
		}
	}

	public function count()
	{
		echo $this->peminjam_model->count();
	}

	public function show($id)
	{
		$data = $this->peminjam_model->detail($id);
	}

	public function update($id)
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'trim|required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('bagian', 'Bagian', 'trim|required');

		if($this->input->post('password', true)) {
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'required');
		}

		$data = [
			'username'      => (string) htmlspecialchars($this->input->post('username', true)),
			'nama'          => (string) htmlspecialchars($this->input->post('nama', true)),
			'telepon'       => (string) htmlspecialchars($this->input->post('telepon', true)),
			'tanggal_lahir' => (string) htmlspecialchars($this->input->post('tanggal_lahir', true)),
			'bagian'        => (string) htmlspecialchars($this->input->post('bagian', true)),
		];

		if($this->input->post('password', true)) {
			$data['password'] = (string) htmlspecialchars($this->input->post('password', true));
		}

		$this->peminjam_model->update($id, $data);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah data peminjam!</div>');
		redirect('peminjam');
	}

	public function delete($id)
	{
		$this->peminjam_model->delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menghapus data peminjam!</div>');
		redirect('peminjam');
	}
}
