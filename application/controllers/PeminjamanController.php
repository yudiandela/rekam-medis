<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeminjamanController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}

		/** Load Library yang di gunakan */
		$this->load->library('form_validation');

		/** Load model yang di gunakan */
		$this->load->model('peminjaman_model');
		$this->load->model('berkas_model');
		$this->load->model('peminjam_model');
	}

	public function index()
	{
		$data['peminjaman'] = $this->peminjaman_model->get();
		$data['berkas'] = $this->berkas_model->get();
		$data['peminjam'] = $this->peminjam_model->get();

		$this->load->view('peminjaman/index', $data);
	}

	public function cetak()
	{
		$data['peminjaman'] = $this->peminjaman_model->get(10);
		$this->load->view('peminjaman/print', $data);
	}

	public function store()
	{
		$this->form_validation->set_rules('id_berkas', 'Berkas', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == false) {
			redirect('peminjaman');
		} else {

			$last = (int) $this->peminjaman_model->count(false) + 1;

			if(strlen($last) == 1) {
				$code = '000';
			} elseif(strlen($last) == 2) {
				$code = '00';
			} elseif(strlen($last) == 3) {
				$code = '0';
			}

			$nomor_peminjaman = (string) 'PMJ' . $code . $last;

			$data = [
				'id_berkas' => (int) htmlspecialchars($this->input->post('id_berkas', true)),
				'keterangan' => (string) htmlspecialchars($this->input->post('keterangan', true)),
				'tanggal' => (string) htmlspecialchars($this->input->post('tanggal', true)) ?: date('Y-m-d'),
				'bagian' => (string) htmlspecialchars($this->input->post('bagian', true)),
				'status' => (string) htmlspecialchars($this->input->post('status', true)),
				'id_peminjam' => (string) htmlspecialchars($this->input->post('peminjam', true)),
				'nomor_peminjaman' => (string) $nomor_peminjaman,
			];

			$this->peminjaman_model->store($data);

			$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menginputkan data peminjaman!</div>');

			redirect('peminjaman');
		}
	}

	public function count()
	{
		echo $this->peminjaman_model->count();
	}

	public function show($id)
	{
		$data = $this->peminjaman_model->detail($id);
	}

	public function update($id)
	{
		$this->form_validation->set_rules('id_berkas', 'Berkas', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		$data = [
			'id_berkas' => (int) htmlspecialchars($this->input->post('id_berkas', true)),
			'keterangan' => (string) htmlspecialchars($this->input->post('keterangan', true)),
			'tanggal' => (string) htmlspecialchars($this->input->post('tanggal', true)) ?: date('Y-m-d'),
			'bagian' => (string) htmlspecialchars($this->input->post('bagian', true)),
			'status' => (string) htmlspecialchars($this->input->post('status', true)),
			'id_peminjam' => (string) htmlspecialchars($this->input->post('peminjam', true))
		];

		$this->peminjaman_model->update($id, $data);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah data peminjaman!</div>');
		redirect('peminjaman');
	}

	public function delete($id)
	{
		$this->peminjaman_model->delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menghapus data peminjaman!</div>');
		redirect('peminjaman');
	}
}
