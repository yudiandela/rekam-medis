<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}
	}

	public function index()
	{
		$this->load->view('home/index');
	}

	/** Custom Errors  */
	public function _404()
	{
		$this->load->view('errors/404');
	}
}
