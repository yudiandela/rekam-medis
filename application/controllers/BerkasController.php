<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BerkasController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}

		/** Load Library yang di gunakan */
		$this->load->library('form_validation');

		/** Load model yang di gunakan */
		$this->load->model('berkas_model');
		$this->load->model('pasien_model');
	}

	public function index()
	{
		$data['berkas'] = $this->berkas_model->get();
		$data['pasien'] = $this->pasien_model->get();

		$this->load->view('berkas/index', $data);
	}

	public function store()
	{
		$this->form_validation->set_rules('nomor_rekam_medis', 'Nomor Rekam Medis', 'trim|required');
		$this->form_validation->set_rules('tanggal_rekam_medis', 'Tanggal Rekam Medis', 'trim|required');
		$this->form_validation->set_rules('resume_keperawatan', 'Resume Keperawatan', 'trim|required');
		$this->form_validation->set_rules('data_obat', 'Data Obat', 'trim|required');
		$this->form_validation->set_rules('data_diagnosa', 'Diagnosa', 'trim|required');
		$this->form_validation->set_rules('id_pasien', 'Pasien', 'trim|required');

		if ($this->form_validation->run() == false) {
			redirect('berkas');
		} else {
			$data = [
				'nomor_rekam_medis'   => (string) htmlspecialchars($this->input->post('nomor_rekam_medis', true)),
				'tanggal_rekam_medis' => (string) htmlspecialchars($this->input->post('tanggal_rekam_medis', true)),
				'resume_keperawatan'  => (string) htmlspecialchars($this->input->post('resume_keperawatan', true)),
				'data_obat'           => (string) htmlspecialchars($this->input->post('data_obat', true)),
				'data_diagnosa'       => (string) htmlspecialchars($this->input->post('data_diagnosa', true)),
				'id_pasien'           => (string) htmlspecialchars($this->input->post('id_pasien', true)),
			];

			$this->berkas_model->store($data);

			$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menginputkan data berkas!</div>');

			redirect('berkas');
		}
	}

	public function count()
	{
		echo $this->berkas_model->count();
	}

	public function show($id)
	{
		$data = $this->berkas_model->detail($id);
	}

	public function update($id)
	{
		$this->form_validation->set_rules('nomor_rekam_medis', 'Nomor Rekam Medis', 'trim|required');
		$this->form_validation->set_rules('tanggal_rekam_medis', 'Tanggal Rekam Medis', 'trim|required');
		$this->form_validation->set_rules('resume_keperawatan', 'Resume Keperawatan', 'trim|required');
		$this->form_validation->set_rules('data_obat', 'Data Obat', 'trim|required');
		$this->form_validation->set_rules('data_diagnosa', 'Diagnosa', 'trim|required');
		$this->form_validation->set_rules('id_pasien', 'Pasien', 'trim|required');

		$data = [
			'nomor_rekam_medis'   => (string) htmlspecialchars($this->input->post('nomor_rekam_medis', true)),
			'tanggal_rekam_medis' => (string) htmlspecialchars($this->input->post('tanggal_rekam_medis', true)),
			'resume_keperawatan'  => (string) htmlspecialchars($this->input->post('resume_keperawatan', true)),
			'data_obat'           => (string) htmlspecialchars($this->input->post('data_obat', true)),
			'data_diagnosa'       => (string) htmlspecialchars($this->input->post('data_diagnosa', true)),
			'id_pasien'           => (string) htmlspecialchars($this->input->post('id_pasien', true)),
		];

		$this->berkas_model->update($id, $data);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah data berkas!</div>');
		redirect('berkas');
	}

	public function delete($id)
	{
		$this->berkas_model->delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menghapus data berkas!</div>');
		redirect('berkas');
	}
}
