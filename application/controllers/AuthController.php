<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function login()
	{
		if($this->session->has_userdata('username') == true) {
			redirect('/');
		}

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('auth/login');
		} else {
			$username = htmlspecialchars($this->input->post('username', true));
			$password = htmlspecialchars($this->input->post('password', true));

			$this->_login($username, $password);
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('bagian');

		$this->session->set_flashdata('message', '<div class="alert alert-success">You have been logged out!</div>');
		redirect('auth/login');
	}

	private function _login($username, $password)
	{
		$user = $this->db->get_where('peminjam', ['username' => $username])->row_array();
		if($user) {
			if($user['password'] == $password) {
				$data = [
					'id' => $user['id_peminjam'],
					'username' => $user['username'],
					'nama' => $user['nama'],
					'bagian' => $user['bagian'],
				];

				$this->session->set_userdata($data);
				redirect('/');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">Password is wrong!</div>');
				redirect('auth/login');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Username not found!</div>');
			redirect('auth/login');
		}
	}
}
