<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller
{
	private $user_login;

	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}

		/** Load Library yang di gunakan */
		$this->load->library('form_validation');

		/** Load model yang di gunakan */
		$this->load->model('peminjam_model');

		/** Mengambil id yang login */
		$this->user_login = $this->peminjam_model->detail($this->session->userdata('id'));
	}

	public function index()
	{
		$data['profile'] = $this->user_login;

		$this->load->view('profile/index', $data);
	}

	public function password()
	{
		$this->form_validation->set_rules('password_lama', 'Password Lama', 'required');
		$this->form_validation->set_rules('password_baru', 'Password Baru', 'required');
		$this->form_validation->set_rules('password_baru_konfirmasi', 'Konfirmasi Password Baru', 'required');

		$password_lama = $this->input->post('password_lama', true);
		$password_baru = $this->input->post('password_baru', true);
		$password_baru_konfirmasi = $this->input->post('password_baru_konfirmasi', true);

		if($password_lama === $this->user_login[0]->password) {
			if($password_baru === $password_baru_konfirmasi) {
				$data = [
					'password' => (string) htmlspecialchars($password_baru),
				];

				$this->peminjam_model->update($this->user_login[0]->id_peminjam, $data);

				$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah password!</div>');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">Konfirmasi password gagal!</div>');
			}

		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Password lama salah!</div>');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function update()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('bagian', 'Bagian', 'required');

		$data = [
			'nama' => (string) htmlspecialchars($this->input->post('nama', true)),
			'username' => (string) htmlspecialchars($this->input->post('username', true)),
			'telepon' => (string) htmlspecialchars($this->input->post('telepon', true)),
			'tanggal_lahir' => (string) htmlspecialchars($this->input->post('tanggal_lahir', true)),
			'bagian' => (string) htmlspecialchars($this->input->post('bagian', true)),
		];

		$this->peminjam_model->update($this->user_login[0]->id_peminjam, $data);

		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah profile!</div>');

		redirect($_SERVER['HTTP_REFERER']);
	}
}
