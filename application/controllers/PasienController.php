<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PasienController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('username') == false) {
			redirect('auth/login');
		}

		/** Load Library yang di gunakan */
		$this->load->library('form_validation');

		/** Load model yang di gunakan */
		$this->load->model('pasien_model');
	}

	public function index()
	{
		$data['pasien'] = $this->pasien_model->get();

		$this->load->view('pasien/index', $data);
	}

	public function store()
	{
		$this->form_validation->set_rules('nama_pasien', 'Nama Pasien', 'trim|required');
		$this->form_validation->set_rules('no_telepon', 'No Telepon', 'trim|required');
		$this->form_validation->set_rules('tanggal_lahir', 'Nanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');

		if ($this->form_validation->run() == false) {
			redirect('pasien');
		} else {
			$data = [
				'nama_pasien'   => (string) htmlspecialchars($this->input->post('nama_pasien', true)),
				'telepon'       => (string) htmlspecialchars($this->input->post('no_telepon', true)),
				'tanggal_lahir' => (string) htmlspecialchars($this->input->post('tanggal_lahir', true)),
				'jenis_kelamin' => (string) htmlspecialchars($this->input->post('jenis_kelamin', true)),
			];

			$this->pasien_model->store($data);

			$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menginputkan data pasien!</div>');

			redirect('pasien');
		}
	}

	public function count()
	{
		echo $this->pasien_model->count();
	}

	public function show($id)
	{
		$data = $this->pasien_model->detail($id);
	}

	public function update($id)
	{
		$this->form_validation->set_rules('nama_pasien', 'Nama Pasien', 'trim|required');
		$this->form_validation->set_rules('no_telepon', 'No Telepon', 'trim|required');
		$this->form_validation->set_rules('tanggal_lahir', 'Nanggal Lahir', 'trim|required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'trim|required');

		$data = [
			'nama_pasien'   => (string) htmlspecialchars($this->input->post('nama_pasien', true)),
			'telepon'       => (string) htmlspecialchars($this->input->post('no_telepon', true)),
			'tanggal_lahir' => (string) htmlspecialchars($this->input->post('tanggal_lahir', true)),
			'jenis_kelamin' => (string) htmlspecialchars($this->input->post('jenis_kelamin', true)),
		];

		$this->pasien_model->update($id, $data);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil mengubah data pasien!</div>');
		redirect('pasien');
	}

	public function delete($id)
	{
		$this->pasien_model->delete($id);
		$this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil menghapus data pasien!</div>');
		redirect('pasien');
	}
}
